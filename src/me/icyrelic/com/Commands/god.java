package me.icyrelic.com.Commands;



import me.icyrelic.com.LegendaryGodMode;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class god implements CommandExecutor {
	
	LegendaryGodMode plugin;
	public god(LegendaryGodMode instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("god")) {
			if(sender instanceof Player){
				Player p = (Player) sender;
				if(sender.hasPermission("LegendaryGodMode.God")){
					if (args.length == 1) { 
						//god other player
						@SuppressWarnings("deprecation")
						Player t = plugin.getServer().getPlayer(args[0]);
						if(t.isOnline()){
							if(LegendaryGodMode.godmode.containsKey(t.getName())){
								if(LegendaryGodMode.godmode.get(t.getName())){
									//true
									plugin.getConfig().set("users."+t.getUniqueId(), null);
									plugin.saveConfig();
									
									LegendaryGodMode.godmode.remove(t.getName());
									LegendaryGodMode.godmode.put(t.getName(), false);
									p.sendMessage(ChatColor.GREEN + t.getName() + "'s God Mode Disabled!");
									t.sendMessage(ChatColor.GREEN + "God Mode Disabled!");
								}else{
									//false
									plugin.getConfig().set("users."+t.getUniqueId(), true);
									plugin.saveConfig();
									LegendaryGodMode.godmode.remove(t.getName());
									LegendaryGodMode.godmode.put(t.getName(), true);
									p.sendMessage(ChatColor.GREEN + t.getName() + "'s God Mode Enabled!");
									t.sendMessage(ChatColor.GREEN + "God Mode Enabled!");
								}
							}else{
								plugin.getConfig().set("users."+t.getUniqueId(), true);
								plugin.saveConfig();
								LegendaryGodMode.godmode.put(t.getName(), true);
								p.sendMessage(ChatColor.GREEN + t.getName() + "'s God Mode Enabled!");
								t.sendMessage(ChatColor.GREEN + "God Mode Enabled!");
							}
						}else{
							p.sendMessage(ChatColor.RED + "Error: No player online by that name!");
						}
		                
					}else{
						//god self
						if(LegendaryGodMode.godmode.containsKey(p.getName())){
							if(LegendaryGodMode.godmode.get(p.getName())){
								//true
								plugin.getConfig().set("users."+p.getUniqueId(), null);
								plugin.saveConfig();
								LegendaryGodMode.godmode.remove(p.getName());
								LegendaryGodMode.godmode.put(p.getName(), false);
								p.sendMessage(ChatColor.GREEN + "God Mode Disabled!");
							}else{
								//false
								plugin.getConfig().set("users."+p.getUniqueId(), true);
								plugin.saveConfig();
								LegendaryGodMode.godmode.remove(p.getName());
								LegendaryGodMode.godmode.put(p.getName(), true);
								p.sendMessage(ChatColor.GREEN + "God Mode Enabled!");
							}
						}else{
							
							plugin.getConfig().set("users."+p.getUniqueId(), true);
							plugin.saveConfig();
							LegendaryGodMode.godmode.put(p.getName(), true);
							p.sendMessage(ChatColor.GREEN + "God Mode Enabled!");
						}
						
					}
				}else{
					sender.sendMessage(ChatColor.RED + "You dont have permission!");
				}
			}else{
				sender.sendMessage(ChatColor.RED + "Only players can run this command!");
			}
		}
		return true;
	}
}


	
	
