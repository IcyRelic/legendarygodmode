package me.icyrelic.com;

import java.util.HashMap;

import me.icyrelic.com.API.LGApi;
import me.icyrelic.com.Commands.god;
import me.icyrelic.com.Data.Updater;
import me.icyrelic.com.Data.Updater.UpdateResult;
import me.icyrelic.com.Data.Updater.UpdateType;
import me.icyrelic.com.Listeners.damage;
import me.icyrelic.com.Listeners.join;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;



public class LegendaryGodMode extends JavaPlugin{

	
	public final LGApi api = new LGApi(this);
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryGodMode" + ChatColor.WHITE + "] ");
	
	public static HashMap<String, Boolean> godmode = new HashMap<String, Boolean>();
	
	public void onEnable(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		loadConfiguration();
		if(getConfig().getBoolean("AutoUpdate")){
			Updater check = new Updater(this, 46796, this.getFile(), UpdateType.NO_DOWNLOAD, true);
			
			if (check.getResult() == UpdateResult.UPDATE_AVAILABLE) {
			    console.sendMessage(prefix+"New Version Available! "+ check.getLatestName());
			    
				Updater download = new Updater(this, 39616, this.getFile(), UpdateType.DEFAULT, true);
				
			    if(download.getResult() == UpdateResult.SUCCESS){
			    	console.sendMessage(prefix+"Successfully Updated Please Restart To Finalize");
			    }
			    
			}else{
				console.sendMessage(prefix+"You are currently running the latest version of LegendaryGodMode");
			}
			


		}
		
		getCommand("god").setExecutor(new god(this));
		getServer().getPluginManager().registerEvents(new damage(this), this);
		getServer().getPluginManager().registerEvents(new join(this), this);

		
		
	}
	
	
	public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	

}
