package me.icyrelic.com.API;

import java.util.HashMap;


import me.icyrelic.com.LegendaryGodMode;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class LGApi {
	public HashMap<String, String> watch = new HashMap<String, String>();
	
	LegendaryGodMode plugin;
	public LGApi(LegendaryGodMode instance) {
		plugin = instance;
	}	
	
	public void addTempGod(Player p, Player p2){
		boolean penabled = false;
		boolean p2enabled = false;
		
		if(!LegendaryGodMode.godmode.containsKey(p.getName())){
			p.sendMessage(ChatColor.RED + "God mode has been temporary enabled for 10 seconds to prevent tp killing");
			LegendaryGodMode.godmode.put(p.getName(), true);
		}else{
			penabled = true;
		}
		if(!LegendaryGodMode.godmode.containsKey(p2.getName())){
			p2.sendMessage(ChatColor.RED + "God mode has been temporary enabled for 10 seconds to prevent tp killing");
			LegendaryGodMode.godmode.put(p2.getName(), true);
		}else{
			p2enabled = true;
		}
		
		
		
		
		
		godExpire(p,p2,penabled,p2enabled);
		
	}
	
	
	public void godExpire(final Player p, final Player p2, final boolean penabled, final boolean p2enabled){
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			  public void run() {
				  
				  
				 
					
						if(LegendaryGodMode.godmode.containsKey(p.getName()) && !penabled){
							 p.sendMessage(ChatColor.RED + "Temporary god mode expired");
							 LegendaryGodMode.godmode.remove(p.getName());
						}
						
						if(LegendaryGodMode.godmode.containsKey(p2.getName()) && !p2enabled){
							 p2.sendMessage(ChatColor.RED + "Temporary god mode expired");
							 LegendaryGodMode.godmode.remove(p2.getName());
						}
						
						

				  
			  }
			}, 300);
	}

}
